//
//  ProfileViewModel.swift
//  MultiCell TableView
//
//  Created by Mark Mokhles on 1/2/19.
//  Copyright © 2019 Emad Asker. All rights reserved.
//

import Foundation

enum ProfileViewModelItemType {
    case nameAndPicture
    case about
    case email
    case friend
    case attribute
}


protocol ProfileViewModelItem{
    var type : ProfileViewModelItemType { get }
    var rowCount : Int { get }
    var sectionTitle : String { get }
}

class ProfileViewModelNameItem : ProfileViewModelItem {
    var type: ProfileViewModelItemType {
        return .nameAndPicture
    }
    
    var sectionTitle: String {
        return "Main Info"
    }
    
    
}


class ProfileViewModelNameAndPictureItem : ProfileViewModelItem {
    
    var type: ProfileViewModelItemType {
        return .nameAndPicture
    }
    
    
    var sectionTitle : String {
        return "Main Info"
    }
    
    var pictureUrl : String
    var userName: String
    init(pictureUrl : String, userName : String){
        self.pictureUrl  = pictureUrl
        self.userName = userName
    }
}


class ProfileViewModelAboutItem {
    
    var type: ProfileViewModelItemType {
        return .about
    }
    
    var sectionTitle : String {
        return "About"
    }
    
    var about : String
    init(about : String ) {
        self.about = about
    }
}

class ProfileViewModelEmailItem{
    
    var type : ProfileViewModelItemType {
        return .email
    }
    
    var sectionTitle: String {
        return "Eamil"
    }
    
    var email: String
    
    init(email : String) {
        self.email = email
    }
    
}

class ProfileViewModelAttributeItem {
    
    var type : ProfileViewModelItemType {
        return .attribute
    }
    
    var sectionTitle: String {
        return "Attributes"
    }
    
    var rowCount:Int {
        return attributes.count
    }
    
    var attributes: [Attribute]
    
    init(attributes: [Attribute]) {
        self.attributes = attributes
    }
}


class ProfileViewModelFriendsItem

extension ProfileViewModelItem {
    
    var rowCount: Int {
        return 1
    }
}
