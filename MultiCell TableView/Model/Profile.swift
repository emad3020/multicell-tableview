//
//  Profile.swift
//  MultiCell TableView
//
//  Created by Mark Mokhles on 1/2/19.
//  Copyright © 2019 Emad Asker. All rights reserved.
//

import Foundation


public func dataFromFile(_ fileName: String) -> Data? {
    @objc class TestClass : NSObject {}
    let bundle = Bundle(for: TestClass.self)
    
    guard let path = bundle.path(forResource: fileName, ofType: "json") else { return nil}
    
    return (try? Data(contentsOf: URL(fileURLWithPath: path)))
    
}

class Profile {
    
    var fullName: String?
    var pictureUrl : String?
    var email : String?
    var about: String?
    var frinds = [Friend]()
    var profileAttributes = [Attribute]()
    
    
    init?(data : Data) {
        do {
            if let json = try JSONSerialization.jsonObject(with: data) as? [String : Any], let body = json["data"] as? [String : Any] {
                self.fullName = body["fullName"] as? String
                self.pictureUrl = body["pictureUrl"] as? String
                self.email = body["email"] as? String
                self.about = body["about"] as? String
                if let friendsArry = body["friends"] as? [[String : Any]] {
                    self.frinds = friendsArry.map { Friend(json: $0) }
                }
                
                if let profileAttributes = body["profileAttributes"] as? [[String : Any]] {
                    self.profileAttributes = profileAttributes.map { Attribute(json: $0) }
                }
                
            }
            
            
        } catch {
            
            print("parsing json error")
            return nil
        }
       
    }
}


class Friend{
    var name : String?
    var pictureUrl : String?
    
    init(json: [String : Any]) {
        self.name = json["name"] as? String
        self.pictureUrl = json["pictureUrl"] as? String
    }
}

class Attribute {
    var key : String?
    var value : String?
    
    
    init(json : [String : Any]) {
        self.key = json["key"] as? String
        self.value = json["value"] as? String
    }
    
}
